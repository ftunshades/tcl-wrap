
#ifndef TCL_WRAP_STL_UTILS_H_
#define TCL_WRAP_STL_UTILS_H_

#include <array>
#include <string>
#include <tuple>
#include <bitset>
#include <utility>

#include "tcl_wrap.h"

namespace tcl_wrap {

template<class Tuple, class S, std::size_t... I>
Tuple parse_tuple(const S& s, std::index_sequence<I...>)
{
        constexpr size_t N = std::tuple_size<Tuple>::value;
        std::vector<std::string_view> tokens;
        tokenize(s, std::back_inserter(tokens));
        if (tokens.size() != N)
                throw std::runtime_error{"expected list of size " + std::to_string(N)};
        return Tuple{Traits<typename std::decay<typename std::tuple_element<I,
                Tuple>::type>::type>::parse(tokens[I])...};
}

template<class Tuple, int N> struct Tuple_emitter {
        using T = typename std::decay<typename std::tuple_element<N, Tuple>::type>::type;
        static std::string emit(const Tuple& t)
        {
                return Tuple_emitter<Tuple, N - 1>::emit(t) + " " +
                        enclose(Traits<T>::emit(std::get<N>(t)));
        }
};

template<class Tuple> struct Tuple_emitter<Tuple, 0> {
        using T = typename std::decay<typename std::tuple_element<0, Tuple>::type>::type;
        static std::string emit(const Tuple& t)
        { return enclose(Traits<T>::emit(std::get<0>(t))); }
};

template<class Tuple> std::string emit_tuple(const Tuple& t)
{
        constexpr size_t N = std::tuple_size<Tuple>::value;
        return Tuple_emitter<Tuple, N - 1>::emit(t);
}

template<class Tuple, int N> struct Name_builder {
        using T = typename std::tuple_element<N, Tuple>::type;
        static std::string name()
        { return Name_builder<Tuple, N - 1>::name() + "," + Traits<T>::name(); }
};

template<class Tuple> struct Name_builder<Tuple, 0> {
        using T = typename std::tuple_element<0, Tuple>::type;
        static std::string name()
        { return Traits<T>::name(); }
};

template<class Tuple> std::string build_name()
{
        constexpr size_t N = std::tuple_size<Tuple>::value;
        return Name_builder<Tuple, N - 1>::name();
}

template<class A, class B> struct Traits<std::pair<A, B>> {
        using Tuple = std::pair<A, B>;
        static std::string name()
        { return "std::pair<" + Traits<A>::name() + "," + Traits<B>::name() + ">"; }
        template<class S> static Tuple parse(const S& s)
        { return parse_tuple<Tuple>(s, std::make_index_sequence<2>{}); }
        static std::string emit(const Tuple& x)
        { return emit_tuple(x); }
};

template<class... Types> struct Traits<std::tuple<Types...>> {
        using Tuple = std::tuple<Types...>;
        static std::string name()
        { return "std::tuple<" + build_name<Tuple>() + ">"; }
        template<class S> static Tuple parse(const S& s)
        { return parse_tuple<Tuple>(s, std::index_sequence_for<Types...>{}); }
        static std::string emit(const Tuple& x)
        { return emit_tuple(x); }
};

template<class T, std::size_t N> struct Traits<std::array<T, N>> {
        using Tuple = std::array<T, N>;
        static std::string name()
        { return "std::array<" + Traits<T>::name() + "," + std::to_string(N) + ">"; }
        template<class S> static Tuple parse(const S& s)
        { return parse_tuple<Tuple>(s, std::make_index_sequence<N>{}); }
        static std::string emit(const Tuple& x)
        { return emit_tuple(x); }
};

template<std::size_t N> struct Traits<std::bitset<N>> {
        using Bitset = std::bitset<N>;
        static std::string name()
        { return "std::bitset<" + std::to_string(N) + ">"; }
        template<class S> static Bitset parse(const S& s)
        { return Bitset(std::string(s)); }
        static std::string emit(const Bitset& b)
        { return b.to_string(); }
};

}; // namespace tcl_wrap

#endif  // TCL_WRAP_STL_UTILS_H_
