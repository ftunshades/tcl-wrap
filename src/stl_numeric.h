
#ifndef TCL_WRAP_STL_NUMERIC_H_
#define TCL_WRAP_STL_NUMERIC_H_

#include <string>

namespace tcl_wrap {

template<class T> struct Traits<T, std::enable_if_t<std::is_integral_v<T>>> {
        static std::string name() { return "integer"; }
        static std::string emit(T value)
        { return std::to_string(value); }
        template<class S> static T parse(const S& s)
        {
                if (!s.empty()) {
                        char* endp;
                        auto x = std::strtoll(s.begin(), &endp, 0);
                        if (endp == s.end()) return x;
                }
                throw std::runtime_error{"expected integer"};
        }
};

template<class T> struct Traits<T, std::enable_if_t<std::is_enum_v<T>>> {
        static std::string name() { return "enumeration"; }
        static std::string emit(T value)
        { return std::to_string(static_cast<long long>(value)); }
        template<class S> static T parse(const S& s)
        {
                if (!s.empty()) {
                        char* endp;
                        auto x = std::strtoll(s.begin(), &endp, 0);
                        if (endp == s.end()) return T(x);
                }
                throw std::runtime_error{"expected enumeration"};
        }
};

template<class T> struct Traits<T, std::enable_if_t<std::is_floating_point_v<T>>> {
        static std::string name() { return "real"; }
        static std::string emit(T x)
        { return std::to_string(x); }
        template<class S> static T parse(const S& s)
        {
                if (!s.empty()) {
                        char* endp;
                        auto x = std::strtold(s.begin(), &endp);
                        if (endp == s.end()) return x;
                }
                throw std::runtime_error{"expected real"};
        }
};

}; // namespace tcl_wrap

#endif  // TCL_WRAP_STL_NUMERIC_H_
