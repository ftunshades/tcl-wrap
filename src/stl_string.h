
#ifndef TCL_WRAP_STL_STRING_H_
#define TCL_WRAP_STL_STRING_H_

#include <string>
#include <string_view>

namespace tcl_wrap {

template<class Char, class S> Char parse_char(const S& s)
{
        if (s.size() != 1)
                throw std::runtime_error{"expected character"};
        return s[0];
}

template<> struct Traits<char> {
        static std::string name() { return "char"; }
        static std::string emit(char x) { return std::string(&x, 1); }
        template<class S> static char parse(const S& s)
        { return parse_char(s); }
};

template<> struct Traits<wchar_t> {
        static std::string name() { return "wchar_t"; }
        static std::string emit(wchar_t x) { return std::string(1, (char)x); }
        template<class S> static wchar_t parse(const S& s)
        { return parse_char(s); }
};

template<class Char, class CTraits, class Allocator>
struct Traits<std::basic_string<Char, CTraits, Allocator>> {
        using String = std::basic_string<Char, CTraits, Allocator>;
        static std::string name()
        { return "std::basic_string<" + Traits<Char>::name() + ">"; }
        static std::string emit(const String& s)
        { return std::string(s.begin(), s.end()); }
        template<class S> static String parse(const S& s)
        { return String(s.begin(), s.end()); }
};

template<class Char, class CTraits>
struct Traits<std::basic_string_view<Char, CTraits>> {
        using String = std::basic_string_view<Char, CTraits>;
        static std::string name()
        { return "std::basic_string_view<" + Traits<Char>::name() + ">"; }
        static std::string emit(const String& s)
        { return std::string(s.begin(), s.end()); }
        template<class S> static String parse(const S& s)
        {
                throw std::runtime_error{"can't create " + name() +
                        " from Tcl string"};
        }
};

template<class CTraits>
struct Traits<std::basic_string_view<char, CTraits>> {
        using String = std::basic_string_view<char, CTraits>;
        static std::string name()
        { return "std::basic_string_view<char>"; }
        static std::string emit(const String& s)
        { return std::string(s.begin(), s.end()); }
        template<class S> static String parse(const S& s)
        { return s; }
};

}; // namespace tcl_wrap

#endif  // TCL_WRAP_STL_STRING_H_
