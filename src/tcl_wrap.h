
#ifndef TCL_WRAP_H_
#define TCL_WRAP_H_

#include <cstring>
#include <algorithm>
#include <functional>
#include <stdexcept>
#include <sstream>
#include <vector>
#include <string_view>

#include "tcl.h"

namespace tcl_wrap {

// Internal_manager and External_manager are helper classes that manage C++
// objects inside or outside the Tcl_Obj object itself, respectively.  Which
// one is to be used depends on the object fitting in the internalRep field of
// Tcl_Obj

template<class T> struct Internal_manager {
        template<class... Args> static void construct(Tcl_Obj* obj, Args&&... args)
        { new (get_internal(obj)) T{std::forward<Args>(args)...}; }
        static T* get_internal(Tcl_Obj* obj)
        { return reinterpret_cast<T*>(&obj->internalRep); }
        static void free_internal(Tcl_Obj* obj)
        { get_internal(obj)->~T(); }
        static void dup_internal(Tcl_Obj* src, Tcl_Obj* dst)
        { new (get_internal(dst)) T{*get_internal(src)}; }
};

template<class T> struct External_manager {
        template<class... Args> static void construct(Tcl_Obj* obj, Args&&... args)
        { obj->internalRep.otherValuePtr = new T{std::forward<Args>(args)...}; }
        static T* get_internal(Tcl_Obj* obj)
        { return reinterpret_cast<T*>(obj->internalRep.otherValuePtr); }
        static void free_internal(Tcl_Obj* obj)
        { delete get_internal(obj); }
        static void dup_internal(Tcl_Obj* src, Tcl_Obj* dst)
        { dst->internalRep.otherValuePtr = new T{*get_internal(src)}; }
};

// The Traits template is a helper class that determines how a C++ is
// serialized and deserialized to and from a Tcl string.  Only classes for
// which the template are specialized can be used is arguments.

// Traits::name returns the name that'll be used to represent the wrapped
// class in Tcl.  It'll be called once in the program lifetime, and a copy
// stored in the name field of a Tcl_ObjType instance.

// Traits::emit returns the string representation for the wrapped class. 
// It'll be called by the update_string function of Tcl_ObjType.

// Traits::parse generates a new internal representation of the wrapped class
// from a string (and raise an exception if it's not possible).
// It'll be called by the set_from_any function of Tcl_ObjType.

template<class T, class Enable = void> struct Traits { };

template<> struct Traits<const char*> {
        static std::string name() { return "const char*"; }
        static std::string emit(const char* x) { return x; }
        template<class S> static void* parse(const S& s)
        { return std::begin(s); }
};

template<> struct Traits<void*> {
        static std::string name()
        { return "void*"; }
        static std::string emit(void* x)
        { return std::to_string((size_t)(x)); }
        template<class S> static void* parse(const S& s)
        {
                char* endp;
                auto x = (void*)std::strtoll(std::begin(s), &endp, 0);
                if (endp != std::end(s))
                        throw std::runtime_error{"expected memory address"};
                return x;
        }
};
template<> struct Traits<bool> {
        static std::string name()
        { return "bool"; }
        static std::string emit(bool x)
        { return x ? "1" : "0"; }
        template<class S> static bool parse(const S& s)
        {
                if (s == "0") return false;
                if (s == "1") return true;
                throw std::runtime_error{"expected boolean"};
        }
};

// The Caster template uses a Manager (internal or external depending on the
// size of the C++ object) and a Traits to implement a Tcl class whose objects
// contain an actual C++ object in their internalRep field.  The functions to
// free, duplicate, serialize, and deserialize the payload are statically
// declared here and pointers to them are held by the (also static) Tcl_ObjType
// returned by type().

// Caster::get returns a pointer to the internal representation of a Tcl_Obj.
// If the object is not of type T, it attempts to change the internal
// representation to be of type T; if it's not possible, it raises an
// exception; if the Tcl_Obj pointer is null, it returns a pointer to a
// default-constructed instance of T (this is useful to fill functions'
// argument lists).

// Caster::set constructs a new Tcl_Obj whose internal representation is a new
// object of type T by forwarding the arguments to T's constructor.  It also
// updates the string representation.

template<class T> class Caster {
 public:
        using Manager = std::conditional_t<
                sizeof(T) <= sizeof(Tcl_Obj::internalRep),
                Internal_manager<T>, External_manager<T>>;

        static T* get(Tcl_Obj* obj)
        {
                if (obj == nullptr)
                        return null();
                if (Tcl_ConvertToType(NULL, obj, type()) != TCL_OK)
                        throw std::bad_cast{};
                return Manager::get_internal(obj);
        }

        template<class... Args> static Tcl_Obj* set(Args&&... args)
        {
                auto obj = Tcl_NewObj();
                try {
                        Manager::construct(obj, std::forward<Args>(args)...);
                        obj->typePtr = type();
                        update_string(obj);
                        return obj;
                } catch (...) {
                        Tcl_DecrRefCount(obj);
                        throw;
                }
        }

 private:
        static void update_string(Tcl_Obj* obj)
        {
                std::string s = Traits<T>::emit(*Manager::get_internal(obj));
                obj->length = s.size();
                obj->bytes = strcpy(Tcl_Alloc(obj->length + 1), s.data());
        }

        static int set_from_any(Tcl_Interp* interp, Tcl_Obj* obj)
        {
                int len;
                auto str = Tcl_GetStringFromObj(obj, &len);
                std::string_view s(str, len);
                Manager::construct(obj, std::move(Traits<T>::parse(s)));
                obj->typePtr = type();
                return TCL_OK;
        }

        static T* null()
        {
                static T impl{};
                return &impl;
        }

        static Tcl_ObjType* type()
        {
                static std::string name = Traits<T>::name();
                static Tcl_ObjType impl = {
                        name.data(),
                        Manager::free_internal,
                        Manager::dup_internal,
                        update_string,
                        set_from_any,
                };
                return &impl;
        };
};

template<> class Caster<char> {
 public:
        static char* get(Tcl_Obj* obj)
        {
                if (obj == nullptr) return null();
                return obj->bytes;
        }

        static Tcl_Obj* set(char s)
        { return Tcl_NewStringObj(&s, 1); }

 private:
        static char* null()
        {
                static char c = '\0';
                return &c;
        }
};

template<> class Caster<const char*> {
 public:
        static const char** get(Tcl_Obj* obj)
        {
                if (obj == nullptr) return null();
                return const_cast<const char**>(&obj->bytes);
        }

        static Tcl_Obj* set(const char* s)
        { return Tcl_NewStringObj(s, -1); }

 private:
        static const char** null()
        {
                static const char* s = "";
                return &s;
        }
};

template<> class Caster<Tcl_Obj> {
 public:
        static Tcl_Obj* get(Tcl_Obj* obj)
        { return obj; }
        static Tcl_Obj* set(const Tcl_Obj& s)
        { throw std::runtime_error{"Tcl_Obj can't be copied in return"}; }
};


template<class T> T* get(Tcl_Obj* obj)
{ return Caster<T>::get(obj); }
template<class T, class... Args> Tcl_Obj* set(Args&&... args)
{ return Caster<T>::set(std::forward<Args>(args)...); }

// The tokenize() function generates a token list from a string view that
// contains tokens separated by spaces and enclosed in curly braces or double
// quotes;  the enclose() function wraps a string in curly braces if it's empty
// or contains spaces.

template<class Iter> Iter tokenize(std::string_view s, Iter out)
{
        for (auto p = s.begin(); p != s.end();) {
                if (isspace(*p)) {
                        ++p;
                } else if (*p == '"') {
                        ++p;
                        auto q = p;
                        p = std::find(p, s.end(), '"');
                        *out++ = std::string_view(q, p - q);
                        ++p;
                } else if (*p == '{') {
                        ++p;
                        auto q = p;
                        int depth = 1;
                        for (;;) {
                                if (p == s.end())
                                        throw std::runtime_error{"missing bracket"};
                                else if (*p == '{')
                                        ++depth;
                                else if (*p == '}')
                                        --depth;
                                if (depth == 0)
                                        break;
                                ++p;
                        }
                        *out++ = std::string_view(q, p - q);
                        ++p;
                } else {
                        auto q = p;
                        p = std::find_if(p, s.end(), isspace);
                        *out++ = std::string_view(q, p - q);
                }
        }
        return out;
}

std::string enclose(std::string s)
{
        if (s.empty() || s.find_first_of(" \t\r\n") != s.npos)
                return "{" + s + "}";
        return s;
}

// Actual Tcl wrapper implementation

// Caller is a template in the return and argument types of a function that
// takes a std::function and a list of Tcl arguments, transforms these
// arguments into the rypes required by the function, calls the function, and
// returns a Tcl object with the return value of the function.

// Wrapper constructs a C++ class around a C++ callable object, mapping its
// arguments and return value to a Tcl_Obj* array and Tcl_Obj pointer,
// respectively.  It also holds an array of default values that can be
// substituted by the passed arguments if they are null.

// Command binds a function like those that result from Wrapper to an actual
// Tcl command.  It automatically manages calling, exception handling, and
// resource destruction for any callable object of the right type passed to it.

template<class R, class... Args> struct Caller {
        template<std::size_t... Is> static Tcl_Obj* call(
                        std::function<R(Args...)>& func,
                        Tcl_Obj* const args[], std::index_sequence<Is...>)
        {
                return set<std::decay_t<R>>(func(std::forward<Args>(
                        *get<std::decay_t<Args>>(args[Is]))...));
        }
};

template<class... Args> struct Caller<void, Args...> {
        template<std::size_t... Is> static Tcl_Obj* call(
                        std::function<void(Args...)>& func,
                        Tcl_Obj* const args[], std::index_sequence<Is...>)
        {
                func(std::forward<Args>(*get<std::decay_t<Args>>(args[Is]))...);
                return nullptr;
        }
};

template<class R, class... Args> class Wrapper {
 public:
        using Func = R(Args...);

        static constexpr int num_args = sizeof...(Args);

        template<class T, class... Defs> Wrapper(T func, Defs... defs)
                : func{std::forward<T>(func)}
        {
                static constexpr int num_defs = sizeof...(Defs);
                static_assert(num_defs <= num_args, "too many default arguments");
                std::array<Tcl_Obj*, num_defs> tmp{set<std::decay_t<Defs>>(
                                std::forward<Defs>(defs))...};
                std::fill(args.begin(), args.end() - num_defs, nullptr);
                std::copy(tmp.begin(), tmp.end(), args.end() - num_defs);
        }

        Wrapper(const Wrapper& other)
                : func{other.func}, args{other.args}
        { for (auto a : args) if (a) Tcl_IncrRefCount(a); }
        Wrapper(Wrapper&& other)
                : func{std::move(other.func)}, args{std::move(other.args)}
        { std::fill(other.args.begin(), other.args.end(), nullptr); }
        Wrapper& operator=(const Wrapper& other) = delete;
        Wrapper& operator=(Wrapper&& other) = delete;
        ~Wrapper()
        { for (auto a : args) if (a) Tcl_DecrRefCount(a); }

        Tcl_Obj* operator()(int objc, Tcl_Obj* const objv[])
        {
                if (objc > num_args)
                        throw std::runtime_error{"too many arguments"};
                Tcl_Obj* argv[num_args];
                std::copy(objv, objv + objc, argv);
                std::copy(&args[objc], args.end(), argv + objc);
                return Caller<R, Args...>::call(func, argv,
                                std::index_sequence_for<Args...>{});
        }

 private:
        std::function<Func> func;
        std::array<Tcl_Obj*, num_args> args;
};

class Command {
 public:
        template<class Func>
        static void bind(Tcl_Interp* interp, const char* name, Func func)
        {
                auto self = new Command{std::forward<Func>(func)};
                Tcl_CreateObjCommand(interp, name, call, self, destroy);
        }

 private:
        std::function<Tcl_Obj*(int, Tcl_Obj* const[])> func;

        template<class Func> Command(Func func)
                : func(std::forward<Func>(func)) {}

        static int call(void* data, Tcl_Interp* interp,
                        int objc, Tcl_Obj* const objv[])
        {
                auto self = static_cast<Command*>(data);
                try {
                        if (auto ret = self->func(objc - 1, objv + 1))
                                Tcl_SetObjResult(interp, ret);
                        return TCL_OK;
                } catch (std::exception& ex) {
                        auto cmd = Tcl_GetCommandFromObj(interp, objv[0]);
                        auto name = Tcl_NewObj();
                        Tcl_GetCommandFullName(interp, cmd, name);
                        Tcl_AppendResult(interp, Tcl_GetString(name) + 2, ": ", ex.what(), NULL);
                        Tcl_DecrRefCount(name);
                        return TCL_ERROR;
                } catch (...) {
                        return TCL_ERROR;
                }
        }

        static void destroy(void *self)
        {
                delete static_cast<Command*>(self);
        }
};

// The actual wrap() functions used to bind C++ stuff to Tcl.

template<class T> void bind(Tcl_Interp* interp, const char* name, const T& x)
{
        Tcl_ObjSetVar2(interp, Tcl_NewStringObj(name, -1), NULL,
                        Caster<T>::set(x), TCL_GLOBAL_ONLY);
}

template<class T> void bind(Tcl_Interp* interp, const char* name, T&& x)
{
        Tcl_ObjSetVar2(interp, Tcl_NewStringObj(name, -1), NULL,
                        Caster<T>::set(std::move(x)), TCL_GLOBAL_ONLY);
}

template<class R, class... Args, class... Defs>
void wrap(Tcl_Interp* interp, const char* name, R(*func)(Args...), Defs... defs)
{ Command::bind(interp, name, Wrapper<R, Args...>{func, defs...}); }

template<class R, class... Args, class... Defs>
void wrap(Tcl_Interp* interp, const char* name, const std::function<R(Args...)>& func, Defs... defs)
{ Command::bind(interp, name, Wrapper<R, Args...>{func, defs...}); }

template<class R, class... Args, class... Defs>
void wrap(Tcl_Interp* interp, const char* name, std::function<R(Args...)>&& func, Defs... defs)
{ Command::bind(interp, name, Wrapper<R, Args...>{std::move(func), defs...}); }

template<class R, class... Args, class Func, class... Defs>
void wrap(Tcl_Interp* interp, const char* name, const Func& func, Defs... defs)
{ Command::bind(interp, name, Wrapper<R, Args...>{func, defs...}); }

template<class R, class... Args, class Func, class... Defs>
void wrap(Tcl_Interp* interp, const char* name, Func&& func, Defs... defs)
{ Command::bind(interp, name, Wrapper<R, Args...>{std::move(func), defs...}); }

}; // namespace tcl_wrap

#endif  // TCL_WRAP_H_
