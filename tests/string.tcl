
load ./libtest.so test

proc test {str} {
        puts "=== $str ==="
        try { puts [typeof [${str}::make]] } on error e { puts $e }
        try { puts [${str}::peek ""] } on error e { puts $e }
        try { puts [${str}::peek "foo"] } on error e { puts $e }
        try { puts [${str}::peek "foo bar"] } on error e { puts $e }
        try { puts [${str}::peek "foo bar baz"] } on error e { puts $e }
}

test std::string
test std::wstring
test std::string_view
test std::wstring_view
