
#include <cmath>
#include <iostream>
#include <memory>

#include "tcl_wrap.h"
#include "stl_containers.h"
#include "stl_memory.h"
#include "stl_numeric.h"
#include "stl_string.h"
#include "stl_utils.h"

// A plain static method with no arguments nor return values

void cmd_hello() { std::cout << "hello, world\n"; }

// A method that returns the type of the raw Tcl object

const char* cmd_typeof(const Tcl_Obj& obj)
{
        if (auto type = obj.typePtr)
                return type->name;
        return nullptr;
}

// A foreach implementation for stl containers that takes as arguments the name
// of the iterating varible, the container, and a block of Tcl code, and runs
// the code once for each element in the container, with the iterator variable
// set to the value of each of the container values.

template<class T> struct cmd_foreach {
 public:
        using V = typename T::value_type;
        cmd_foreach(Tcl_Interp* interp) : interp{interp} {}
        void operator()(const char* iter, const T& cont, Tcl_Obj& func)
        {
                for (auto& x : cont) {
                        auto y = tcl_wrap::Traits<V>::emit(x);
                        Tcl_SetVar(interp, iter, y.data(), TCL_NAMESPACE_ONLY);
                        if (Tcl_EvalObjEx(interp, &func, 0) != TCL_OK) {
                                Tcl_UnsetVar(interp, iter, TCL_NAMESPACE_ONLY);
                                throw std::runtime_error{""};
                        }
                }
                Tcl_UnsetVar(interp, iter, TCL_NAMESPACE_ONLY);
        }

 private:
        Tcl_Interp* interp;
};

// Some functions that are just used to test different ways to pass values

template<class T> T cmd_make() { return T{}; }
template<class T> std::string cmd_peek(const T& x)
{ return tcl_wrap::Traits<T>::emit(x); }
template<class T> std::string cmd_take(T&& x)
{ return tcl_wrap::Traits<T>::emit(std::move(x)); }
template<class T> T cmd_copy(const T& x) { return x; }
template<class T> T cmd_move(T&& x) { return std::move(x); }

// The Foo class notifies each time it is default-constructed, destructed,
// copied, or moved; mostly useful to test value passing.

struct Foo {
        Foo() { std::cout << "Foo::Foo()\n"; }
        Foo(const Foo& other) { std::cout << "Foo::Foo(const Foo&)\n"; }
        Foo(Foo&& other) { std::cout << "Foo::Foo(Foo&&)\n"; }
        ~Foo() { std::cout << "Foo::~Foo()\n"; }
        Foo& operator=(const Foo& other)
        {
                std::cout << "Foo& Foo::operator=(const Foo&)\n";
                return *this;
        }
        Foo& operator=(Foo&& other)
        {
                std::cout << "Foo& Foo::operator=(Foo&&)\n";
                return *this;
        }
};

// These functions are used to test default argument values

const char* str_or_foo(const char* str) { return str; }
int int_or_42(int i) { return i; }
double double_or_pi(double d) { return d; }

namespace tcl_wrap {
template<> struct Traits<Foo> {
        static std::string name() { return "Foo"; }
        static std::string emit(const Foo& x) { return "Foo"; }
        template<class S> static Foo parse(const S& s)
        {
                if (s != "Foo")
                        throw std::runtime_error{"expected Foo"};
                return Foo{};
        }
};

}; // namespace tcl_wrap

// Generate functions to operate on type T with name N

template<class T> void wrap_type(Tcl_Interp* interp, const std::string& name)
{
        tcl_wrap::wrap(interp, (name + "::make").data(), cmd_make<T>);
        tcl_wrap::wrap(interp, (name + "::peek").data(), cmd_peek<T>);
        tcl_wrap::wrap(interp, (name + "::take").data(), cmd_take<T>);
        tcl_wrap::wrap(interp, (name + "::copy").data(), cmd_copy<T>);
        tcl_wrap::wrap(interp, (name + "::move").data(), cmd_move<T>);
}

template<class T> void wrap_cont(Tcl_Interp* interp, const std::string& name)
{
        wrap_type<T>(interp, name);
        tcl_wrap::wrap<void, const char*, const T&, Tcl_Obj&>(interp,
                        (name + "::foreach").data(), cmd_foreach<T>(interp));
}

// All wrap declarations

extern "C" int Test_Init(Tcl_Interp* interp)
{
        Tcl_PkgProvide(interp, "test", "1.0.0");

        tcl_wrap::wrap(interp, "typeof", cmd_typeof);
        tcl_wrap::wrap(interp, "hello", cmd_hello);
        wrap_type<const char*>(interp, "str");
        wrap_type<char>(interp, "char");
        wrap_type<signed char>(interp, "ssint");
        wrap_type<short>(interp, "sint");
        wrap_type<int>(interp, "int");
        wrap_type<long>(interp, "lint");
        wrap_type<long long>(interp, "llint");
        wrap_type<unsigned char>(interp, "ussint");
        wrap_type<unsigned short>(interp, "usint");
        wrap_type<unsigned>(interp, "uint");
        wrap_type<unsigned long>(interp, "ulint");
        wrap_type<unsigned long long>(interp, "ullint");
        tcl_wrap::wrap(interp, "std::malloc", malloc);
        tcl_wrap::wrap(interp, "std::free", free);
        wrap_type<Foo>(interp, "foo");
        wrap_type<std::shared_ptr<Foo>>(interp, "foop");
        wrap_type<std::string>(interp, "std::string");
        wrap_type<std::wstring>(interp, "std::wstring");
        wrap_type<std::string_view>(interp, "std::string_view");
        wrap_type<std::wstring_view>(interp, "std::wstring_view");

        wrap_type<std::pair<std::string, std::string>>(interp, "std::pair");
        wrap_type<std::tuple<std::string>>(interp, "std::tuple1");
        wrap_type<std::tuple<std::string, std::string>>(interp, "std::tuple2");
        wrap_type<std::tuple<std::string, std::string, std::string>>(interp, "std::tuple3");
        wrap_type<std::array<std::string, 1>>(interp, "std::array1");
        wrap_type<std::array<std::string, 2>>(interp, "std::array2");
        wrap_type<std::array<std::string, 3>>(interp, "std::array3");
        wrap_cont<std::vector<std::string>>(interp, "std::vector");
        wrap_cont<std::deque<std::string>>(interp, "std::deque");
        wrap_cont<std::list<std::string>>(interp, "std::list");
        wrap_cont<std::set<std::string>>(interp, "std::set");
        wrap_cont<std::unordered_set<std::string>>(interp, "std::uset");
        wrap_cont<std::multiset<std::string>>(interp, "std::mset");
        wrap_cont<std::unordered_multiset<std::string>>(interp, "std::umset");
        wrap_cont<std::map<std::string, std::string>>(interp, "std::map");
        wrap_cont<std::unordered_map<std::string, std::string>>(interp, "std::umap");
        wrap_cont<std::multimap<std::string, std::string>>(interp, "std::mmap");
        wrap_cont<std::unordered_multimap<std::string, std::string>>(interp, "std::ummap");
        tcl_wrap::wrap<int, int>(interp, "int::abs", std::abs);
        tcl_wrap::wrap(interp, "lint::abs", std::labs);
        tcl_wrap::wrap(interp, "llint::abs", std::llabs);
        tcl_wrap::wrap<float, float>(interp, "float::abs", std::abs);
        tcl_wrap::wrap<double, double>(interp, "lfloat::abs", std::abs);
        tcl_wrap::wrap<long double, long double>(interp, "llfloat::abs", std::abs);
        tcl_wrap::wrap<float, float, float>(interp, "float::mod", std::fmod);
        tcl_wrap::wrap<double, double, double>(interp, "lfloat::mod", std::fmod);
        tcl_wrap::wrap<long double, long double, long double>(interp, "llfloat::mod", std::fmod);

        tcl_wrap::wrap(interp, "str_or_foo", str_or_foo, "foo");
        tcl_wrap::wrap(interp, "int_or_42", int_or_42, 42);
        tcl_wrap::wrap(interp, "double_or_pi", double_or_pi, M_PI);

        return TCL_OK;
}

