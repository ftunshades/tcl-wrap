
load ./libtest.so test

puts "=== foo::make ==="
puts [foo::make]

puts "=== foo::copy ==="
puts [foo::copy Foo]

puts "=== foo::move ==="
puts [foo::move Foo]

puts "=== foo::peek ==="
foo::peek Foo

puts "=== foo::take ==="
foo::take Foo
