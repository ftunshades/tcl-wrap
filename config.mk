
# Where this file resides

root_dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# When version changes, increment this number

VERSION := 1.0.0

# Determine where we are going to install things

prefix := /usr/local
suffix := $(shell echo $(VERSION) | grep -o '^[0-9]*')
bindir := $(prefix)/bin
libdir := $(prefix)/lib
includedir := $(prefix)/include/tcl_wrap
datadir := $(prefix)/share/tcl_wrap
docdir := $(prefix)/share/doc/tcl_wrap
htmldir := $(docdir)
pdfdir := $(docdir)
localstatedir := $(prefix)/var/tcl_wrap

# There are so many tools needed...

DIFF := diff
INSTALL := install
INSTALL_PROGRAM := $(INSTALL)
INSTALL_DATA := $(INSTALL) -m 644

# These flags may be overriden by the user

CXXFLAGS := -g

# These flags are necessary

override CXXFLAGS += -std=c++17
override CXXFLAGS += -MMD
override CXXFLAGS += -fPIC
override CXXFLAGS += -Werror
override CXXFLAGS += -Wfatal-errors

# To go down a level $(call descend,directory[,target[,flags]])

descend = make -C $(1) $(2) $(3)

# If the -s option was passed, use these to give a hint of what we are doing

ifneq ($(findstring s,$(filter-out --%,$(MAKEFLAGS))),)
    QUIET_CC =		@echo "CC        $@";
    QUIET_LINK =	@echo "LINK      $@";
    QUIET_AR =		@echo "AR        $@";
    QUIET_GEN =		@echo "GEN       $@";
    QUIET_UNZIP =	@echo "UNZIP     $@";
    QUIET_TEST =	@echo "TEST      $@";
    descend =		@echo "ENTER     `pwd`/$(1)";\
			 make -C $(1) $(2) $(3);\
			 echo "EXIT      `pwd`/$(1)"
endif

# Some generic targets that are the same for all Makefiles
.obj/%.o: %.c | .obj
	$(QUIET_CC)$(CC) $(CPPFLAGS) $(CFLAGS) -c -o $@ $<

.obj/%.o: %.cc | .obj
	$(QUIET_CXX)$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c -o $@ $<

.obj:
	mkdir $@

%: .obj/%.o
	$(QUIET_LINK)$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS)

%.so:
	$(QUIET_LINK)$(CC) $(LDFLAGS) -shared -Wl,-soname,$@ -o $@ $^ $(LDLIBS)

%: %.c
%: %.cc

